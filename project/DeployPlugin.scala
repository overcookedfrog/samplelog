import collection.JavaConverters._
import sbt._
import Keys._
import scala.sys.process._
import java.nio.file.{ Files, Path, Paths, StandardCopyOption }
import java.nio.file.attribute.PosixFilePermission._
import better.files.{File => BFile}


object DeployPlugin extends AutoPlugin {                                                                                                                                                                                   

  override def trigger = allRequirements

  object autoImport {
    val deployDirectory = settingKey[String]("deployment directory")
    val deployMain = settingKey[Map[String, String]]("main classes map (prog_name -> main class)")
    val buildDeploy = taskKey[Unit]("")
    val archiveTgz = taskKey[Unit]("Create gz compressed tar archive")
  }

  import autoImport._

  override lazy val projectSettings = Seq(
    deployDirectory := s"${target.value}/deploy",
    deployMain := Map[String, String](),
    buildDeploy := {
      import sbtassembly.AssemblyKeys._
      assembly.value
      val out = streams.value
      val jarPath = BFile((assemblyOutputPath in assembly).value.toString)
      val jarFile = (assemblyJarName in assembly).value
      val javaOpts = Vector(
        "-Xmx2g",
        "-XX:+UseParallelOldGC",
        "-XX:ParallelGCThreads=4",
        "-XX:GCTimeLimit=50",
        "-XX:GCHeapFreeLimit=1",
        s"-Dprog.name=${name.value}",
        s"-Dprog.version=${version.value}").mkString(" ")


      val dd = BFile(deployDirectory.value.toString)
      if (dd.exists && dd.isDirectory) {
        out.log.info(s"deleting existing deploy directory: $dd")
        dd.delete()
      }

      val bin = BFile(s"$dd/bin")
      val lib = BFile(s"$dd/lib")

      bin.createDirectories()
      lib.createDirectories()

      jarPath.copyToDirectory(lib)

      val mainTable: Map[String, String] = deployMain.value

      for ((name, mainClass) <- mainTable) {
        val localScript = BFile(s"${deployDirectory.value}/bin/${name}")
        localScript
          .createIfNotExists()
          .appendLines(
            "#!/usr/bin/env sh",
            """homedir="$(cd "$(dirname "$0")" && cd ".." && pwd -P)"""",
            s"""exec java ${javaOpts} -cp $${homedir}/lib/${jarFile} ${mainClass} \"$$@\"""",
            "")
        localScript.setPermissions(Set(OWNER_READ, OWNER_EXECUTE, GROUP_READ, GROUP_EXECUTE, OTHERS_READ, OTHERS_EXECUTE))

      }

      out.log.info(s"build deployment directory at ${deployDirectory.value}")
    },
    archiveTgz := {
      buildDeploy.value
      val out = streams.value
      val archiveName = s"${target.value}/${name.value}-${version.value}.tar.gz"
      val d = new File(deployDirectory.value).getParent
      val f = new File(deployDirectory.value).getName
      val cmd = s"""tar --create -z -C ${d} -f ${archiveName} --transform s/^${f}/${name.value}-${version.value}/ ${f}"""
      cmd.!
      out.log.info(s"Created archive at ${archiveName}")
    }
  )

}
