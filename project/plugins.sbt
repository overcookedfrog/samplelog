addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.9.3")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")


addSbtPlugin("com.sludg.sbt" % "gitlab" % "0.5")

libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.5.0"
