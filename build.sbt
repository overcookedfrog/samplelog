enablePlugins(GitVersioning, BuildInfoPlugin, GitlabPlugin)

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.gitlab.overcookedfrog",
      scalaVersion := "2.12.7",
      scalacOptions := Seq("-deprecation", "-feature")
    )),
    name := "samplelog",
    libraryDependencies ++= Seq(
      "org.apache.poi" % "poi" % "4.1.0",
      "org.apache.poi" % "poi-ooxml" % "4.1.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.rogach" %% "scallop" % "3.3.1",
      "org.apache.commons" % "commons-csv" % "1.5",
      "com.typesafe" % "config" % "1.3.4"
    ),
    crossScalaVersions := List("2.12.7", "2.13.1")
  )

git.useGitDescribe := true
//git.uncommittedSignifier := Some("dirty")

val commitHash = settingKey[String]("")
commitHash := git.gitHeadCommit.value.getOrElse("missing-git-commit")
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, commitHash)
buildInfoPackage := s"${organization.value}.${name.value}"
buildInfoOptions += BuildInfoOption.ToMap
buildInfoOptions += BuildInfoOption.ToJson


deployMain := Map(
  "samplelog-tsv" -> "atgc.samplelog.Main"
)

/*publishTo := Some("Gitlab Samplelog" at "https://gitlab.com/api/v4/projects/10824190/packages/maven")*/
/*credentials += Credentials(Path.userHome / ".sbt" / ".credentials")*/

com.sludg.sbt.gitlab.GitlabPlugin.gitlabProjectId := "14586227"
