package atgc.samplelog

import scala.util.Try
import java.time.LocalDate

/**
  * ClinicalSample is the same as Sample except that required fields are not
  * [[Option]]
  */
class ClinicalSample(
  sampleId: String,
  subjectId: String,
  name: String,
  dob: LocalDate,
  auslab: Option[String],
  emr: Option[String],
  anatomicalPathology: Option[String],
  ur: String,
  fin: Option[String],
  sex: String,
  tissueSourceType: String,
  preservationType: String,
  sampleType: String,
  nucleicType: String,
  disease: Option[String],
  reportType: String,
  receivedDate: LocalDate,
  consentReceivedDate: LocalDate,
  loggedBy: Option[String],
  portionId: Option[String],
  projectId: Option[String])


object ClinicalSample {

  def fromSample(sample: Sample): Try[ClinicalSample] = Try {

    //val sampleId = sample.sampleId.getOrElse(throw new SampleLogException(s"sample has no sample ID"))
    val subjectId = sample.subjectId.getOrElse(throw new SampleLogException("sample has no subject ID"))
    val name = sample.name.getOrElse(throw new SampleLogException("Sample has no name"))
    val dob = sample.dob.getOrElse(throw new SampleLogException("Sample has no DOB"))
    val ur = sample.ur.getOrElse(throw new SampleLogException("Sample has no URN"))
    val sex = sample.sex.getOrElse(throw new SampleLogException("Sample has no sex"))
    val tissueSourceType = sample.tissueSourceType.getOrElse(throw new SampleLogException("Sample has no tissue_source_site"))
    val preservationType = sample.preservationType.getOrElse(throw new SampleLogException("Sample has no preservation_type"))
    val sampleType = sample.sampleType.getOrElse(throw new SampleLogException("Sample has no sample_type"))
    val nucleicType = sample.nucleicType.getOrElse(throw new SampleLogException("Sample has no nucleic_type"))
    val reportType = sample.reportType.getOrElse(throw new SampleLogException("Sample has no report_type"))
    val receivedDate = sample.receivedDate.getOrElse(throw new SampleLogException("Sample has no received_date"))
    val consentReceivedDate = sample.consentReceivedDate.getOrElse(throw new SampleLogException("Sample has no consent_received_date"))

    new ClinicalSample(
      sample.sampleId, subjectId, name, dob, sample.auslab, sample.emr, sample.anatomicalPathology, ur, sample.fin, 
      sex, tissueSourceType, preservationType, sampleType, nucleicType, sample.disease, reportType, receivedDate, 
      consentReceivedDate, sample.loggedBy, sample.portionId, sample.projectId)
  }

}
