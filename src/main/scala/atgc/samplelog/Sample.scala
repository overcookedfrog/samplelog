package atgc.samplelog 

import java.time.LocalDate


case class Sample(
  sampleId: String,
  subjectId: Option[String],
  name: Option[String],
  dob: Option[LocalDate],
  auslab: Option[String],
  emr: Option[String],
  anatomicalPathology: Option[String],
  ur: Option[String],
  fin: Option[String],
  sex: Option[String],
  tissueSourceType: Option[String],
  preservationType: Option[String],
  sampleType: Option[String],
  nucleicType: Option[String],
  disease: Option[String],
  reportType: Option[String],
  sampleCollectionDate: Option[LocalDate],
  receivedDate: Option[LocalDate],
  consentReceivedDate: Option[LocalDate],
  loggedBy: Option[String],
  portionId: Option[String],
  projectId: Option[String])

