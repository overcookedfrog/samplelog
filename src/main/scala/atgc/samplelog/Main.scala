package atgc.samplelog


import scala.util.{ Success, Failure }
import java.io.{ File, PrintStream }
import java.nio.file.Paths
import org.rogach.scallop._


class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val noPII = opt[Boolean]()
  val output = trailArg[String]()
  verify()
}

object Main {

  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    SampleLog.load match {
      case Success(db) => 
        db.toCsv(Paths.get(conf.output()), conf.noPII())
      case Failure(err) => 
        System.err.println(err)
        System.exit(1)
    }
  }

}
