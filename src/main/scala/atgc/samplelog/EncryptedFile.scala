package atgc.samplelog 

import java.nio.file.Path

case class EncryptedFile(path: Path, passwd: Option[String])
