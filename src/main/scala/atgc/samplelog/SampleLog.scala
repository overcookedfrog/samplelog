package atgc.samplelog 

import scala.util.Failure
import scala.util.Properties
import scala.util.Success
import scala.util.Try
import collection.JavaConverters._
import collection.mutable.ArrayBuffer
import java.nio.charset.Charset
import java.nio.file.{ Path, Paths, Files }
import java.time.format.{ DateTimeFormatter, DateTimeParseException }
import java.time.LocalDate
import java.text.SimpleDateFormat
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger
import org.apache.commons.csv.{ CSVFormat, CSVPrinter }
import org.apache.poi.ss.usermodel._


class MissingColumnException(message: String) extends SampleLogException(message)
class InvalidDateException(message: String) extends SampleLogException(message)

case class SampleLog(samples: Seq[Sample]) {

  private[this] val logger = Logger("SampleLog")

  lazy val dateFormat = {
    val x = Try {
      val userConfigFile = Paths.get(Properties.userHome, ".config", "atgc", "atgc.conf")
      val config = ConfigFactory.parseFile(userConfigFile.toFile).resolve()
      config.getString("atgc.samplelog.date-format")
    }
    DateTimeFormatter.ofPattern(x.getOrElse("yyyy-MM-dd"))
  }

  private[this] val m = samples.map(i => i.sampleId -> i).toMap

  private[this] val bySubject = samples.groupBy(_.subjectId)

  def forUid(id: String): Option[Sample] = m.get(id)

  private[this] def validConsentReceivedDate(sample: Sample): Boolean = sample.consentReceivedDate.isDefined

  private[this] def validDob(sample: Sample): Boolean = sample.dob.isDefined


  def forClinicalUid(id: String): Try[ClinicalSample] = forUid(id) match {
    case Some(sample) => ClinicalSample.fromSample(sample)
    case None => Failure(new SampleLogException(f"missing sample fo UID $id"))
  }

  def forSubject(id: String): Option[Seq[Sample]] = bySubject.get(Some(id))

  private[this] def sampleToVector(x: Sample, noPII: Boolean): Vector[String] = noPII match {
    case true =>
      Vector(
        x.sampleId,
        x.subjectId.getOrElse(""),
        x.sex.getOrElse(""),
        x.tissueSourceType.getOrElse(""),
        x.preservationType.getOrElse(""),
        x.sampleType.getOrElse(""),
        x.disease.getOrElse(""),
        x.reportType.getOrElse(""),
        x.sampleCollectionDate.map(_.format(dateFormat)).getOrElse(""),
        x.receivedDate.map(_.format(dateFormat)).getOrElse(""),
        x.consentReceivedDate.map(_.format(dateFormat)).getOrElse(""),
        x.loggedBy.getOrElse(""),
        x.portionId.getOrElse(""),
        x.projectId.getOrElse(""))
    case _ =>
      Vector(
        x.sampleId,
        x.subjectId.getOrElse(""),
        x.name.getOrElse(""),
        x.dob.map(_.format(dateFormat)).getOrElse(""),
        x.auslab.getOrElse(""),
        x.emr.getOrElse(""),
        x.anatomicalPathology.getOrElse(""),
        x.ur.getOrElse(""),
        x.fin.getOrElse(""),
        x.sex.getOrElse(""),
        x.tissueSourceType.getOrElse(""),
        x.preservationType.getOrElse(""),
        x.sampleType.getOrElse(""),
        x.disease.getOrElse(""),
        x.reportType.getOrElse(""),
        x.sampleCollectionDate.map(_.format(dateFormat)).getOrElse(""),
        x.receivedDate.map(_.format(dateFormat)).getOrElse(""),
        x.consentReceivedDate.map(_.format(dateFormat)).getOrElse(""),
        x.loggedBy.getOrElse(""),
        x.portionId.getOrElse(""),
        x.projectId.getOrElse(""))
  }

  // PII - personal identifiable information
  def toCsv(out: Path, noPII: Boolean): Unit = {
    val bufferedWriter = Files.newBufferedWriter(out, Charset.forName("UTF-8"))
    try {
      toCsv(bufferedWriter, noPII)
    } finally {
      bufferedWriter.close()
    }
  }

  def toCsv(out: Appendable, noPII: Boolean): Unit = {
    //val columns = config.getStringList("atgc.samplelog.columns").asScala
    val columns = noPII match {
      case true => List(
        "sample_id",
        "subject_id",
        "sex",
        "tissue_source",
        "preservation_method",
        "sample_type",
        "disease",
        "report_type",
        "sample_collection_date",
        "received_date",
        "consent_received_date",
        "logged_by",
        "portion_id",
        "project_id")
      case _ => List(
        "sample_id",
        "subject_id",
        "name",
        "dob",
        "auslab",
        "emr",
        "anatomical_pathology",
        "ur",
        "fin",
        "sex",
        "tissue_source",
        "preservation_method",
        "sample_type",
        "disease",
        "report_type",
        "sample_collection_date",
        "received_date",
        "consent_received_date",
        "logged_by",
        "portion_id",
        "project_id")
    }
    val format = CSVFormat.DEFAULT.withHeader(columns:_*)
    val writer = new CSVPrinter(out, format)
    for (sample <- this.samples) {
      writer.printRecord(sampleToVector(sample, noPII):_*)
    }
  }

}


object SampleLog {

  private[this] val logger = Logger("SampleLog")

  private[this] val formatter = new DataFormatter(new java.util.Locale("en", "AU"))

  private[this] implicit class RichRow(row: Row) {

    lazy val wb = row.getSheet.getWorkbook
    //lazy val style = wb.createCellStyle
    //style.setDataFormat(wb.getCreationHelper.createDataFormat.getFormat("yyyy-mm-dd"))
    //lazy val evaluator = wb.getCreationHelper.createFormulaEvaluator
    
    private def stringValue(cell: Cell): String = {
      if (cell != null) {
        cell.getCellType match {
          case CellType.STRING => cell.getRichStringCellValue().getString
          case CellType.NUMERIC =>
            if (DateUtil.isCellDateFormatted(cell)) {
              val f = new SimpleDateFormat("yyyy-MM-dd")
              f.format(cell.getDateCellValue())
            } else {
              cell.getNumericCellValue.toString
            }
          case CellType.BOOLEAN =>
            cell.getBooleanCellValue().toString
          case CellType.FORMULA =>
            cell.getCachedFormulaResultType match {
              case CellType.STRING => cell.getRichStringCellValue().toString
              case CellType.NUMERIC =>
                if (DateUtil.isCellDateFormatted(cell)) {
                  val f = new SimpleDateFormat("yyyy-MM-dd")
                  f.format(cell.getDateCellValue())
                } else {
                  cell.getNumericCellValue().toString
                }
              case CellType.BOOLEAN => cell.getBooleanCellValue().toString
              case _ => ""
            }
          case CellType.BLANK => ""
          case _ => ""
        }
      } else {
        ""
      }
    }

    lazy val headerMap: Map[String, Int] = {
      val m = collection.mutable.Map[String, Int]()
      val header = row.getSheet.getRow(2)
      for ((cell, idx) <- header.asScala.zipWithIndex) {
        //val tmp = formatter.formatCellValue(cell, evaluator)
        val tmp = stringValue(cell)
        if (!(tmp == "" || tmp == "NA")) {
          m += (tmp -> idx)
        }
      }
      m.toMap
    }
    
    //def sampleId: [String] = Some(row.getCell(1).getStringCellValue)
    
    def isEmpty(): Boolean = {
      for (c <- row.getFirstCellNum() to row.getLastCellNum()) {
        val cell = row.getCell(c)
        if (cell != null && cell.getCellType != CellType.BLANK) {
          return false
        }
      }
      true
    }

    private val PROJECT_ID_COLUMN = "ProjectID"

    def projectId: Option[String] = {
      val cell = row.getCell(headerMap(PROJECT_ID_COLUMN))
      val v = stringValue(cell)
      v match {
        case "" | "NA" => None
        case _ => Some(v)
      }
    }

    private[this] def formattedCell(i: Int): Option[String] = {
      val cell = row.getCell(i)
      val v = stringValue(cell)
      v match {
        case "" | "NA" => None
        case _ => Some(v)
      }
    }

    def isValidSampleId(): Boolean = {
      val sampleid = raw"(\d{8}-\d{4})".r
      row.sampleId match {
        case sampleid(id) => true
        case _ => false
      }
    }

    def isValidRow(): Boolean = isValidSampleId
    
    def sampleId = try { 
      formattedCell(1).get 
    } catch { 
      case err: java.util.NoSuchElementException => 
        "<MISSING SAMPLE ID>"
    }

    // Virtually everything is an Option[String] where it will be None even if
    // the column is not defined in the file. It has to be this way to
    // accomodate research samples, but it makes any kind of error checking
    // impossible.

    def loggedBy = headerMap.get("Logged by").flatMap(formattedCell)
    def emr = headerMap.get("EMR").flatMap(formattedCell).map(i => Try(f"${i.toDouble}%.0f").getOrElse(i))
    def auslab = headerMap.get("Auslab").flatMap(formattedCell).map(i => Try(f"${i.toDouble}%.0f").getOrElse(i))
    def anatomicalPathology = headerMap.get("Anatomical Pathology").flatMap(formattedCell)
    def ur = headerMap.get("UR").flatMap(formattedCell).map(i => Try(f"${i.toDouble}%.0f").getOrElse(i))
    //def name = row.getCell(9).getStringCellValue
    def name = headerMap.get("PatientName").flatMap(formattedCell)
    def sex = {
      val i = headerMap.get("Gender").flatMap(formattedCell)
      i match {
        case Some(s) =>
          if (s != "MALE" && s != "FEMALE") {
            //logger.warn(s"unexpected sex $s ($sampleId)")
          }
        case None => /* do nothing */
      }
      i
    }
    def subjectId = formattedCell(headerMap("SubjectID")).map(i => Try(f"${i.toDouble}%.0f").getOrElse(i))
    def tissueSourceType = headerMap.get("TissueSourceType").flatMap(formattedCell)
    def preservationType = {
      val i = headerMap.get("TissuePreservationType").flatMap(formattedCell)
      i match {
        case Some(s) =>
          if (s != "Fresh" && s != "FFPE") {
            //logger.warn(s"unexpected preservation_method $s ($sampleId)")
          }
        case None => /* do nothing */
      }
      i
    }
    def sampleType = {
      val i = headerMap.get("SampleType").flatMap(formattedCell)
      i match {
        case Some(s) =>
          if (s != "Tumour" && s != "Normal") {
            //logger.warn(s"unexpected sample_type $s ($sampleId)")
          }
        case None => /* do nothing */
      }
      i
    }
    def nucleicType = {
      val i = headerMap.get("NucleicType").flatMap(formattedCell)
      i match {
        case Some(s) =>
          if (s != "DNA" && s != "RNA") {
            //logger.warn(s"unexpected nucleic_type $s ($sampleId)")
          }
        case None => /* do nothing */
      }
      i
    }
    def portionId = headerMap.get("Portion").flatMap(formattedCell) match {
      case Some(id) =>
        if (('A' to 'Z').map(_.toString).contains(id)) Some(id)
        else None
      case None => 
        None
    }
    def disease = headerMap.get("Disease").flatMap(formattedCell)
    def reportType = headerMap.get("ReportType").flatMap(formattedCell)
    def fin = headerMap.get("FIN").flatMap(formattedCell).map(i => Try(f"${i.toDouble}%.0f").getOrElse(i))

    private[this] def dateString(x: String): Option[String] = try {
      val bits = x.split("-+")
      val day = if (bits(0).length == 1) s"0${bits(0)}" else bits(0)
      val month = bits(1).toLowerCase.capitalize
      val year = bits(2)
      Some(s"$day-$month-$year")
    } catch {
      case _: java.lang.ArrayIndexOutOfBoundsException => None
    }

    /**
      * dd-MMM-yyyy will not handle 01-JAN-80 (i.e., date in uppercase)
      */
    def str2date(x: String): Try[Option[LocalDate]] = Try {
      val date = raw"(\d{1,2})-(\w{3})-(\d{2,4})".r
      val date2 = raw"(\d{4})-(\d{2})-(\d{2})".r
      val date3 = raw"(\d{1,2})/(\d{1,2})/(\d{4})".r
      x match {
        case date(d, m, y) =>
          val day = f"${d.toInt}%02d"
          val month = m.toLowerCase.capitalize
          val year = {
            if (y.length == 2) {
              val i = LocalDate.now.getYear.toString.substring(2, 4).toInt
              val j = if (y.toInt <= i) {
                s"20$y" 
              } else {
                s"19$y"
              }
              //logger.warn(s"year too short $x, guessing $j")
              j
            } else {
              y
            }
          }
          val s = f"$day-$month-$year"
          Some(LocalDate.parse(s, DateTimeFormatter.ofPattern("dd-MMM-yyyy")))
        case date2(y, m, d) =>
          Some(LocalDate.of(y.toInt, m.toInt, d.toInt))
        case date3(d, m, y) =>
          Some(LocalDate.of(y.toInt, m.toInt, d.toInt))
        case _ =>
          throw new InvalidDateException(x)
      }
    }

    def dateCell(i: Int): Try[Option[LocalDate]] = {
      val cell = row.getCell(i)
      val v = stringValue(cell)
      v match {
        case "" | "NA" | "NOT" | "Not" => Success(None)
        case _ => str2date(v)
      }
    }

    def dob: Try[Option[LocalDate]] = {
      headerMap.get("DOB") match {
        case Some(idx) => dateCell(idx)
        case None => Failure(new MissingColumnException(""""DOB""""))
      }
    }
    //def dob: String = formattedCell(10)
    def sampleCollectionDate: Try[Option[LocalDate]] = {
      headerMap.get("Sample Collection Date") match {
        case Some(idx) => dateCell(idx)
        case None => Failure(new MissingColumnException(""""Sample Collection Date""""))
      }
    }
    def receivedDate: Try[Option[LocalDate]] = {
      headerMap.get("Receipt date") match {
        case Some(idx) => dateCell(idx)
        case None => Failure(new MissingColumnException(""""Receipt date""""))
      }
    }
    def consentReceivedDate: Try[Option[LocalDate]] = {
      headerMap.get("Consent Received date") match {
        case Some(idx) => dateCell(idx)
        case None => Failure(new MissingColumnException(""""Consent Received date""""))
      }
    }
    
    // FIXME: a lot of earlier samples do not have a constent received date,
    // how do we know that there is consent?
    //def hasConsent: Boolean = {
    //  //!(consentReceivedDate == "NOT" || consentReceivedDate == "Not" || consentReceivedDate == "NA")
    //}
    def hasConsent: Boolean = consentReceivedDate match {
      case Success(x) => x match {
        case Some(y) => true
        case None => false
      }
      case Failure(err) => false
    }

  }

  private[this] def sampleDb(f: EncryptedFile): Try[SampleLog] = Try {
    val workbook = f.passwd match {
      case Some(passwd) => WorkbookFactory.create(f.path.toFile, passwd)
      case None => WorkbookFactory.create(f.path.toFile)
    }

    val sheetOp: Option[Sheet] = Option(workbook.getSheet("ATG Sample Log")) match {
      case Some(sheet) => Some(sheet)
      case None =>
        Option(workbook.getSheet("Research Sample Log")) match {
          case Some(sheet) => Some(sheet)
          case None => 
            println(System.err.println(s"missing sheet in ${f.path.toString}"))
            None
        }
    }
    val sheet = sheetOp.get
    //val evaluator = workbook.getCreationHelper.createFormulaEvaluator
    val samples = ArrayBuffer[Sample]()

    for (i <- 3 to sheet.getLastRowNum) {
      val row = sheet.getRow(i)
      if (row != null && row.isValidRow) {

        val dob = row.dob match {
          case Success(x) => x
          case Failure(err) => 
            err match {
              case e: MissingColumnException =>
                None
              case e: InvalidDateException =>
                throw new SampleLogException(f"""(DOB): ${err.getMessage} ${row.sampleId} ${f.path}""")
            }
        }
        val sampleCollectionDate = row.sampleCollectionDate match {
          case Success(x) => x
          case Failure(err) => 
            err match {
              case e: MissingColumnException =>
                None
              case e: InvalidDateException =>
                throw new SampleLogException(f"""(Sample Collection Date): ${err.getMessage} ${row.sampleId} ${f.path}""")
            }
        }
        val receivedDate = row.receivedDate match {
          case Success(x) => x
          case Failure(err) => 
            err match {
              case e: MissingColumnException => 
                None
              case e: InvalidDateException =>
                throw new SampleLogException(f"""(Received date): ${err.getMessage} ${row.sampleId} ${f.path}""")
            }
        }
        val consentReceivedDate = row.consentReceivedDate match {
          case Success(x) => x
          case Failure(err) => 
            err match {
              case e: MissingColumnException =>
                None
              case e: InvalidDateException =>
                throw new SampleLogException(f"""(Consent Received Date): ${err.getMessage} ${row.sampleId} ${f.path}""")
            }
        }
        val sample = Sample(
          row.sampleId,
          row.subjectId,
          row.name,
          dob, //"2018-11-12", //row.dob,
          row.auslab,
          row.emr,
          row.anatomicalPathology,
          row.ur,
          row.fin,
          row.sex,
          row.tissueSourceType,
          row.preservationType,
          row.sampleType,
          row.nucleicType,
          row.disease,
          row.reportType,
          sampleCollectionDate,
          receivedDate,
          consentReceivedDate,
          row.loggedBy,
          row.portionId,
          row.projectId
        )
        samples += sample
      }
    }                     
    workbook.close()
    SampleLog(samples.toSeq)
  }

  /**
    * The sample log files are all password protected. We will require each
    * user to create a config file with the paths and passwords to use this
    * class.
    */
  def load: Try[SampleLog] = Try {

    val userConfigFile = Paths.get(Properties.userHome, ".config", "atgc", "atgc.conf")

    if (!Files.exists(userConfigFile))
      throw new SampleLogException(s"missing user config file: $userConfigFile")

    val userConfig = ConfigFactory.parseFile(userConfigFile.toFile).resolve()

    val files = userConfig.getConfigList("atgc.samplelog.samplelogs").asScala map { x =>
      val password = if (x.hasPath("password")) Some(x.getString("password")) else None
      EncryptedFile(Paths.get(x.getString("path")), password)
    }
    files foreach { f =>
      logger.info(f"Reading samples log: ${f.path}")
    }
    //SampleLog(files.map(f => sampleDb(f).samples).flatten)
    val samples = ArrayBuffer[Sample]()
    for (file <- files) {
      sampleDb(file) match {
        case Success(db) => samples ++= db.samples
        case Failure(err) => throw err
      }
    }

    val duplicateSamples = samples.map(_.sampleId).toList.groupBy(identity).collect { case (x, List(_, _, _*)) => x }
    if (duplicateSamples.size > 0) {
      duplicateSamples.foreach { uid =>
        logger.error(f"""duplicate UID found $uid""")
      }
      logger.error(f"""found ${duplicateSamples.size} duplicate UIDs""")
    }
    SampleLog(samples.toSeq)
  }

}
